import phonenumbers
from phonenumbers import PhoneNumber, NumberParseException


class PhoneNumberParseError(Exception):
    pass


def parse_phone_number(value: str, default_region='RU') -> PhoneNumber:
    try:
        value = phonenumbers.parse(value, default_region)
        if not phonenumbers.is_valid_number(value):
            raise PhoneNumberParseError
        return value
    except (NumberParseException, AttributeError) as ex:
        raise PhoneNumberParseError from ex


def as_international(phone: PhoneNumber) -> str:
    return phonenumbers.format_number(phone, phonenumbers.PhoneNumberFormat.INTERNATIONAL)
