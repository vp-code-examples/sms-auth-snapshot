from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed

User: AbstractUser = get_user_model()


class APIKeyAuthentication(BaseAuthentication):
    def authenticate(self, request):
        api_key_from_get = request.GET.get('api_key')
        api_key_from_header = request.META.get('HTTP_AUTHORIZATION')
        if api_key_from_header:
            api_key_from_header = api_key_from_header.split(' ', 1)
            if not len(api_key_from_header) == 2 or not api_key_from_header[0] == 'API':
                raise AuthenticationFailed('Неверный формат заголовка.')
            api_key_from_header = api_key_from_header[1].strip()

        if not (api_key_from_header or api_key_from_get):
            return None

        if api_key_from_header and api_key_from_get:
            raise AuthenticationFailed('Ошибка. API-ключ имеется как в заголовке так и в GET-запросе.')

        api_key = api_key_from_header or api_key_from_get
        try:
            user = User.objects.get(is_active=True, client_profile__api_key=api_key)
            return user, None
        except User.DoesNotExist:
            raise AuthenticationFailed('Неверный API-ключ.')
