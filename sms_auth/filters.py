from django_filters import UUIDFilter, BaseInFilter, FilterSet, CharFilter


class UUIDInFilter(BaseInFilter, UUIDFilter):
    pass


class CharInFilter(BaseInFilter, CharFilter):
    pass


class SmsAuthSessionFilter(FilterSet):
    auth_session_uuid__in = UUIDInFilter(field_name='auth_session_uuid', lookup_expr='in')
    status__in = CharInFilter(field_name='status', lookup_expr='in')
    domain__in = CharInFilter(field_name='domain__name', lookup_expr='in')
