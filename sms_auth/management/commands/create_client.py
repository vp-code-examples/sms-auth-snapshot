import json
import logging

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.management import BaseCommand
from django.db import transaction
from django.utils import timezone
from gsmmodem import GsmModem
from gsmmodem.exceptions import TimeoutException
from gsmmodem.modem import ReceivedSms
from serial import SerialException, time

from sms_auth.models import ReceivedSms, ClientProfile, AuthDomain

logger = logging.getLogger('create_client')

User: AbstractUser = get_user_model()


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--name', dest='name', action='store', required=True)
        parser.add_argument('--key', dest='api_key', action='store', required=True)
        parser.add_argument('--domain', dest='domain', action='store', default=None)

    def handle(self, *args, **options):
        with transaction.atomic():
            user = User.objects.create(username=options['name'], first_name=options['name'],
                                       is_active=True, is_staff=False, is_superuser=False)
            profile = ClientProfile.objects.create(user=user, api_key=options['api_key'])

            if options['domain']:
                AuthDomain.objects.create(client_profile=profile, name=options['domain'])

        self.stdout.write(f'Клиент {user.username} успешно создан.')
