import logging

from django.conf import settings
from django.core.management import BaseCommand
from django.utils import timezone
from gsmmodem import GsmModem
from gsmmodem.exceptions import TimeoutException
from gsmmodem.modem import ReceivedSms as ModemReceivedSms
import redis
from serial import SerialException, time

from lib.phone import parse_phone_number, as_international, PhoneNumberParseError
from sms_auth.models import ReceivedSms
from sms_auth.session import try_auth

logger = logging.getLogger('sms_auth.modemd')

SLEEP_INTERVAL = 1

redis_client = redis.from_url(settings.REDIS_URL)
redis_client.ping()


def handle_sms(sms: ModemReceivedSms):
    logger.info('Получено SMS-сообщение от %s с текстом "%s"', sms.number, sms.text)
    try:
        phone = parse_phone_number(sms.number)
    except PhoneNumberParseError:
        logger.error('Неизвестный формат номера "%s"', sms.number)
        return

    sms = ReceivedSms.objects.create(
        phone=as_international(phone),
        received_at=sms.time,
        text=sms.text[:4096],
    )
    try_auth(sms)
    # redis_client.publish('sms_auth:modemd:sms-received', json.dumps({'id': sms_rec.id}))


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--simulator', dest='simulator', action='store_true')
        parser.add_argument('--phone', dest='phone', action='store', default='')
        parser.add_argument('--text', dest='text', action='store', default='')

    def handle(self, *args, **options):
        if options['simulator']:
            self._handle_simulator(options['phone'], options['text'])
        else:
            self._handle_modem()

    def _handle_modem(self):
        logger.info('Инициализация модема...')
        modem = GsmModem(settings.GSM_MODEM_PORT, settings.GSM_MODEM_BAUDRATE, smsReceivedCallbackFunc=handle_sms)
        modem.smsTextMode = False
        try:
            modem.connect()
            logger.info('Ожидание входящих SMS')
        except (SerialException, TimeoutException):
            logger.exception('Нет связи с модемом')
            exit(1)

        while True:
            if not modem.alive:
                logger.error('Нет связи с модемом')
                exit(1)
            time.sleep(SLEEP_INTERVAL)

    def _handle_simulator(self, phone: str, text: str):
        class FakeModem:
            pass

        sms = ModemReceivedSms(
            gsmModem=FakeModem(),
            number=phone,
            text=text,
            time=timezone.now(),
            status=ModemReceivedSms.STATUS_RECEIVED_UNREAD,
        )
        handle_sms(sms)
