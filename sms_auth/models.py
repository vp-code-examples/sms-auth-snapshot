import uuid

from django.contrib.auth import get_user_model
from django.db import models

SHOW_SMS_MAX_NCHARS = 32

User = get_user_model()


class ReceivedSms(models.Model):
    class Meta:
        db_table = 'sms_auth_received_sms'

    phone = models.CharField(max_length=256, blank=True)

    received_at = models.DateTimeField()

    text = models.CharField(max_length=4096, blank=True)

    def __str__(self):
        return f'{self.phone} "{self.text[:SHOW_SMS_MAX_NCHARS]}{"..." if len(self.text) > SHOW_SMS_MAX_NCHARS else ""}"'


class ClientProfile(models.Model):
    class Meta:
        db_table = 'sms_auth_client_profile'

    user = models.OneToOneField(User, related_name='client_profile', on_delete=models.DO_NOTHING)

    api_key = models.CharField(max_length=64, unique=True, db_index=True)


class AuthDomain(models.Model):
    class Meta:
        db_table = 'sms_auth_domain'
        unique_together = [('name', 'client_profile')]

    name = models.CharField(
        max_length=16,
        help_text='Имя домена',
        db_index=True,
    )

    client_profile = models.ForeignKey(
        'ClientProfile', null=True, on_delete=models.DO_NOTHING, related_name='domains',
    )


class SmsAuthSession(models.Model):
    class Meta:
        db_table = 'sms_auth_session'

    class Status(models.TextChoices):
        PENDING = 'PENDING', 'Ожидание SMS'
        PASSED = 'PASSED', 'Аутентификация прошла успешно'
        ERR_WRONG_AUTH_CODE = 'ERR_WRONG_AUTH_CODE', 'Аутентификация не удалась (неверный код)'
        ERR_CANCELLED = 'ERR_CANCELLED', 'Отмена'
        ERR_EXPIRED = 'ERR_EXPIRED', 'Аутентификация не удалась (сессия истекла)'

    class Method(models.TextChoices):
        SMS_FROM_CLIENT = 'SMS_FROM_CLIENT', 'Отправка SMS со стороны клиента'
        SMS_FROM_CLIENT_DEFER_PHONE = 'SMS_FROM_CLIENT_DEFER_PHONE', 'Отправка SMS со стороны клиента без указания телефона'

    auth_session_uuid = models.UUIDField(
        default=uuid.uuid4, db_index=True,
        help_text='Уникальный идентификатор сессии аутентификации (UUIDv4)'
    )

    method = models.CharField(
        max_length=32,
        choices=Method.choices,
        help_text='Метод аутентификации',
    )

    domain = models.ForeignKey(
        'AuthDomain', null=True, on_delete=models.DO_NOTHING, related_name='domains',
        help_text='Домен аутентификации (имя службы, для которой производится аутентификация)',
    )

    client_phone = models.CharField(
        max_length=256,
        blank=True,
        help_text='Телефон клиента в формате +7 XXX XXXXXXX',
    )

    auth_phone = models.CharField(
        max_length=256,
        help_text='Телефон службы аутентификации в формате +7 XXX XXXXXXX (показывать клиенту)',
    )

    auth_code = models.IntegerField(help_text='Код аутентификации (внутренний)',)

    qualified_auth_code = models.CharField(
        max_length=24,
        default='',
        help_text='Код аутентификации (показывать клиенту)',
    )

    created_at = models.DateTimeField(
        help_text='Дата и время создания сессии',
    )

    expire_at = models.DateTimeField(
        help_text='Дата и время истечения сессии',
    )

    status = models.CharField(
        max_length=32,
        choices=Status.choices,
        help_text='Состояние сессии',
    )

    sms = models.OneToOneField('ReceivedSms', null=True, on_delete=models.SET_NULL, related_name='auth_sessions')
