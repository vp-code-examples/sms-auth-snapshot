from django.conf import settings
from rest_framework import serializers
from rest_framework.relations import SlugRelatedField
from rest_framework.serializers import ModelSerializer

from sms_auth.models import SmsAuthSession


class SmsAuthSessionSerializer(ModelSerializer):
    domain = SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = SmsAuthSession
        exclude = ['id', 'sms', 'auth_code']


class CreateAuthSessionRequestSerializer(serializers.Serializer):
    domain = serializers.CharField(
        required=True, max_length=256,
        help_text='Домен аутентификации (имя службы, для которой производится аутентификация)'
    )

    client_phone = serializers.CharField(
        default=None, max_length=256,
        help_text='Телефон клиента в формате +7 XXX XXXXXXX'
    )

    method = serializers.ChoiceField(
        choices=SmsAuthSession.Method.choices,
        help_text='Метод аутентификации',
    )

    prefer_short_code = serializers.BooleanField(
        default=False,
        help_text='Использовать числовой код без домена, если нет активных сессий в других доменах'
    )

    ttl = serializers.IntegerField(
        required=False, min_value=settings.MIN_AUTH_SESSION_TTL, max_value=settings.MAX_AUTH_SESSION_TTL,
        help_text='Время жизни сессии в секундах (если не указано expire_at)'
    )

    expire_at = serializers.DateTimeField(
        required=False,
        help_text='Дата и время истечения сессии (если не указано ttl)'
    )
