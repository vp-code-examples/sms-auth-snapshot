import logging

from sms_auth.models import ReceivedSms, SmsAuthSession

SMS_CORRELATION_DELAY = 120  # seconds

logger = logging.getLogger('sms_auth.session')


def make_qualified_auth_code(domain, auth_code, short=False):
    if short:
        return str(auth_code)
    return f'{domain.lower()}{auth_code}'


def normalize_auth_code(value):
    value = value.strip()
    value = ''.join(value.split())
    return value.lower()


def try_auth(sms: ReceivedSms):
    qualified_auth_code = normalize_auth_code(sms.text)
    try:
        session = SmsAuthSession.objects.get(
            status=SmsAuthSession.Status.PENDING,
            qualified_auth_code=qualified_auth_code
        )

        assert session.method in [SmsAuthSession.Method.SMS_FROM_CLIENT, SmsAuthSession.Method.SMS_FROM_CLIENT_DEFER_PHONE]
        if session.method == SmsAuthSession.Method.SMS_FROM_CLIENT:
            if session.client_phone != sms.phone:
                logger.error('Ошибка авторизации по SMS <%s>', sms)
                return
        elif session.method == SmsAuthSession.Method.SMS_FROM_CLIENT_DEFER_PHONE:
            session.client_phone = sms.phone
        session.sms = sms
        session.status = session.Status.PASSED
        session.save()
        logger.info('Авторизация сессии #%s по SMS <%s> прошла успешно', session.id, sms)
    except SmsAuthSession.DoesNotExist:
        logger.error('Ошибка авторизации по SMS <%s>', sms)
