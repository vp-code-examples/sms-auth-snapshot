from django.utils import timezone

from .celery import app
from .models import SmsAuthSession


@app.task(name='check-expired-sessions')
def check_expired_sessions():
    expired_session_ids = SmsAuthSession.objects.filter(
        expire_at__lt=timezone.now(),
        status=SmsAuthSession.Status.PENDING,
    ).values_list('id', flat=True)

    # TODO: Emit MQ message here with 'expired_session_ids'

    if not expired_session_ids:
        return

    SmsAuthSession.objects.filter(id__in=expired_session_ids).update(status=SmsAuthSession.Status.ERR_EXPIRED)
