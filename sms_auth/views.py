import random
from datetime import timedelta
from uuid import UUID

from django.conf import settings
from django.db import transaction
from django.db.models import Subquery
from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.mixins import ListModelMixin, CreateModelMixin, DestroyModelMixin, RetrieveModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from lib.phone import as_international, parse_phone_number, PhoneNumberParseError
from sms_auth.filters import SmsAuthSessionFilter
from sms_auth.models import SmsAuthSession, AuthDomain
from sms_auth.serializers import SmsAuthSessionSerializer, CreateAuthSessionRequestSerializer
from sms_auth.session import make_qualified_auth_code


class SmsAuthSessionViewSet(ListModelMixin, RetrieveModelMixin, CreateModelMixin, DestroyModelMixin, GenericViewSet):
    serializer_class = SmsAuthSessionSerializer
    lookup_field = 'auth_session_uuid'
    filter_class = SmsAuthSessionFilter

    def get_queryset(self):
        return SmsAuthSession.objects.select_related('domain') \
            .order_by('-id') \
            .filter(domain__client_profile__user=self.request.user)

    @swagger_auto_schema(
        operation_id='Список сессий аутентификации',
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_id='Детали сессии аутентификации',
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(
        operation_id='Открыть сессию аутентификации',
        request_body=CreateAuthSessionRequestSerializer(),
        responses={
            200: SmsAuthSessionSerializer
        }
    )
    def create(self, request, *args, **kwargs):
        ser = CreateAuthSessionRequestSerializer(data=request.data)
        if not ser.is_valid():
            return Response({'errors': ser.errors}, status=status.HTTP_400_BAD_REQUEST)

        method = ser.validated_data.get('method')
        client_phone = ser.validated_data.get('client_phone') or ''

        assert method in [SmsAuthSession.Method.SMS_FROM_CLIENT, SmsAuthSession.Method.SMS_FROM_CLIENT_DEFER_PHONE]
        if method == SmsAuthSession.Method.SMS_FROM_CLIENT:
            if not client_phone:
                return Response({'detail': 'Укажите телефонный номер'},
                                status=status.HTTP_400_BAD_REQUEST)
            try:
                client_phone = parse_phone_number(client_phone)
            except PhoneNumberParseError:
                return Response({'detail': 'Неверный формат телефонного номера'}, status=status.HTTP_400_BAD_REQUEST)

            client_phone = as_international(client_phone)
        elif method == SmsAuthSession.Method.SMS_FROM_CLIENT_DEFER_PHONE:
            if client_phone:
                return Response({'detail': 'Номер телефона не нужен при данном методе аутентификации'},
                                status=status.HTTP_400_BAD_REQUEST)

        created_at = timezone.now()
        expire_at = ser.validated_data.get('expire_at')

        if ser.validated_data.get('ttl') is not None:
            if expire_at is not None:
                return Response({'detail': 'Укажите ttl или expire_at, но не оба сразу'},
                                status=status.HTTP_409_CONFLICT)
            expire_at = created_at + timedelta(seconds=ser.validated_data['ttl'])
        else:
            expire_at = expire_at
            if expire_at is None:
                return Response({'detail': 'Укажите ttl или expire_at'},
                                status=status.HTTP_400_BAD_REQUEST)
            expire_at = expire_at

        if expire_at - timedelta(seconds=settings.MIN_AUTH_SESSION_TTL) < created_at:
            return Response(
                {'detail': f'Время жизни сессии должно быть не менее {settings.MIN_AUTH_SESSION_TTL} секунд'},
                status=status.HTTP_400_BAD_REQUEST
            )

        if expire_at > (created_at + timedelta(seconds=settings.MAX_AUTH_SESSION_TTL)):
            return Response(
                {'detail': f'Время жизни сессии должно быть не более {settings.MAX_AUTH_SESSION_TTL} секунд'},
                status=status.HTTP_400_BAD_REQUEST
            )

        try:
            domain = AuthDomain.objects.get(client_profile__user=request.user, name=ser.validated_data['domain'])
        except AuthDomain.DoesNotExist:
            return Response(
                {'detail': f'Домен "{ser.validated_data["domain"]}" не существует'},
                status=status.HTTP_400_BAD_REQUEST
            )

        with transaction.atomic():
            codes_occupied = set(SmsAuthSession.objects.filter(status=SmsAuthSession.Status.PENDING)
                                 .values_list('auth_code', flat=True))
            auth_code = random.choice([code for code in settings.AUTH_CODES_RANGE if code not in codes_occupied])

            has_pending_sessions_in_other_domain = SmsAuthSession.objects \
                .filter(domain__client_profile__user=request.user, status=SmsAuthSession.Status.PENDING) \
                .exclude(domain=domain) \
                .select_for_update() \
                .exists()

            use_short_code = ser.validated_data['prefer_short_code'] and not has_pending_sessions_in_other_domain

            session = SmsAuthSession.objects.create(
                client_phone=client_phone,
                method=method,
                status=SmsAuthSession.Status.PENDING,
                created_at=created_at,
                expire_at=expire_at,
                domain=domain,
                auth_phone=settings.GSM_MODEM_PHONE,
                auth_code=auth_code,
                qualified_auth_code=make_qualified_auth_code(domain.name, auth_code, short=use_short_code)
            )

        session = SmsAuthSessionSerializer(instance=session)

        return Response(session.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_id='Отмена сессии аутентификации',
    )
    def destroy(self, request, *args, **kwargs):
        try:
            UUID(kwargs['auth_session_uuid'], version=4)
        except ValueError:
            return Response({'detail': 'Неверный формат UUID.'}, status=status.HTTP_404_NOT_FOUND)

        try:
            with transaction.atomic():
                sess = SmsAuthSession.objects.select_for_update() \
                    .filter(domain__client_profile__user=request.user) \
                    .only('status') \
                    .get(auth_session_uuid=kwargs['auth_session_uuid'])

                if sess.status == SmsAuthSession.Status.PENDING:
                    sess.status = SmsAuthSession.Status.ERR_CANCELLED
                    sess.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except SmsAuthSession.DoesNotExist:
            return Response({'detail': 'Не найдено.'}, status=status.HTTP_404_NOT_FOUND)
