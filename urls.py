'''sms_auth URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
'''
from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import routers, permissions

from sms_auth.views import SmsAuthSessionViewSet

schema_view = get_schema_view(
    openapi.Info(
        title='API SMS-Аутентификации',
        default_version='v1',
        description='',
        # terms_of_service='',
        contact=openapi.Contact(email='mail@atlant-ankey.ru'),
        # license=openapi.License(name=''),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = routers.SimpleRouter()
router.register(r'auth/auth-sessions', SmsAuthSessionViewSet, basename='auth-session')

urlpatterns = [
    *router.urls,
    path('', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
